# 5. Recuperación de registros específicos (select . where)

Hemos aprendido cómo ver todos los registros de una tabla:

select nombre, clave from usuarios;
El comando "select" recupera los registros de una tabla. Detallando los nombres de los campos separados por comas, indicamos que seleccione todos los campos de la tabla que nombramos.

Existe una cláusula, "where" que es opcional, con ella podemos especificar condiciones para la consulta "select". Es decir, podemos recuperar algunos registros, sólo los que cumplan con ciertas condiciones indicadas con la cláusula "where". Por ejemplo, queremos ver el usuario cuyo nombre es "MarioPerez", para ello utilizamos "where" y luego de ella, la condición:

select nombre, clave from usuarios where nombre='MarioPerez';
Para las condiciones se utilizan operadores relacionales (tema que trataremos más adelante en detalle). El signo igual(=) es un operador relacional. Para la siguiente selección de registros especificamos una condición que solicita los usuarios cuya clave es igual a 'bocajunior':

select nombre, clave from usuarios where clave='bocajunior';
Si ningún registro cumple la condición establecida con el "where", no aparecerá ningún registro.

Ingresemos los siguientes comandos SQL:

```sql
drop table if exists usuarios;

create table usuarios (
  nombre varchar(30),
  clave varchar(10)
);
```

describe usuarios;

```sql
insert into usuarios (nombre, clave) values ('Leonardo','payaso');
insert into usuarios (nombre, clave) values ('MarioPerez','Marito');
insert into usuarios (nombre, clave) values ('Marcelo','bocajunior');
insert into usuarios (nombre, clave) values ('Gustavo','bocajunior');

select nombre, clave from usuarios;

select nombre, clave from usuarios where nombre='Leonardo';

select nombre, clave from usuarios where clave='bocajunior';

select nombre, clave from usuarios where clave='river';
```

# Ejercicios de laboratororio

Trabajamos con la tabla "usuarios" que consta de 2 campos: nombre de usuario y clave.
Eliminamos la tabla si ya existe:

```sql
drop table usuarios;
```

Creamos la tabla:

```sql
create table usuarios (
    nombre varchar2(30),
    clave varchar2(10)
);
```

Vemos la estructura de la tabla:

```sql
describe usuarios;
```

Ingresamos algunos registros:

```sql
insert into usuarios (nombre, clave) values ('Marcelo','Boca');
insert into usuarios (nombre, clave) values ('JuanPerez','Juancito');
insert into usuarios (nombre, clave) values ('Susana','River');
insert into usuarios (nombre, clave) values ('Luis','River');
```

Realizamos una consulta especificando una condición, queremos ver toda la información del usuario cuyo nombre es "Marcelo":

```sql
select *from usuarios where nombre='Marcelo';
```

Nos muestra todos los campos del registro en el cual el campo "nombre" es igual a "Marcelo".

Queremos ver el nombre de los usuarios cuya clave es "River":

```sql
select nombre from usuarios where clave='River';
```

Nos muestra 2 usuarios.

Realizamos un "select" de los nombres de los usuarios cuya clave es "Santi":

```sql
select nombre from usuarios where clave='Santi';
```

No se muestra ningún registro ya que ninguno cumple la condición.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table usuarios;
 
create table usuarios (
    nombre varchar2(30),
    clave varchar2(10)
);

describe usuarios;

insert into usuarios (nombre, clave) values ('Marcelo','Boca');
insert into usuarios (nombre, clave) values ('JuanPerez','Juancito');
insert into usuarios (nombre, clave) values ('Susana','River');
insert into usuarios (nombre, clave) values ('Luis','River');

select * from usuarios where nombre='Marcelo';

select nombre from usuarios where clave='River';

select nombre from usuarios where clave='Santi';
```

# Ejercicios propuestos

## Ejercicio 01

Trabaje con la tabla "agenda" en la que registra los datos de sus amigos.

1. Elimine "agenda"

2. Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```

3. Visualice la estructura de la tabla "agenda" (4 campos)

4. Ingrese los siguientes registros ("insert into"):

```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');
```

5. Seleccione todos los registros de la tabla (5 registros)

6. Seleccione el registro cuyo nombre sea "Marisa" (1 registro)

7. Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)

8. Seleccione los nombres y domicilios de quienes tengan apellido igual a "lopez" (en minúsculas)

No aparece ningún registro, ya que la cadena "Lopez" no es igual a la cadena "lopez".

9. Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)

## Ejercicio 02

Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla llamada "articulos".

1. Elimine la tabla si existe.

2. Cree la tabla "articulos" con la siguiente estructura:

```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2)
);
```

3. Vea la estructura de la tabla:

```sql
 describe articulos;
```

4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
insert into articulos (codigo, nombre, descripcion, precio) values (4,'teclado','ingles Biswal',100);
insert into articulos (codigo, nombre, descripcion, precio) values (5,'teclado','español Biswal',90);
```

5. Seleccione todos los datos de los registros cuyo nombre sea "impresora" (2 registros)

6. Muestre sólo el código, descripción y precio de los teclados (2 registros)
