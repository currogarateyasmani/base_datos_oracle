# 23. Ordenar registros (order by)

Podemos ordenar el resultado de un "select" para que los registros se muestren ordenados por algún campo, para ello usamos la cláusula "order by".

La sintaxis básica es la siguiente:

```sql
select * from NOMBRETABLA
order by CAMPO;
```

Por ejemplo, recuperamos los registros de la tabla "libros" ordenados por el título:

```sql
select * from libros
order by titulo;
```

Aparecen los registros ordenados alfabéticamente por el campo especificado.

También podemos colocar el número de orden del campo por el que queremos que se ordene en lugar de su nombre, es decir, referenciar a los campos por su posición en la lista de selección. Por ejemplo, queremos el resultado del "select" ordenado por "precio":

```sql
select titulo, autor, precio
from libros order by 3;
```

Si colocamos un número mayor a la cantidad de campos de la lista de selección, aparece un mensaje de error y la sentencia no se ejecuta.

Por defecto, si no aclaramos en la sentencia, los ordena de manera ascendente (de menor a mayor). Podemos ordenarlos de mayor a menor, para ello agregamos la palabra clave "desc":

```sql
select * libros
order by editorial desc;
```

También podemos ordenar por varios campos, por ejemplo, por "titulo" y "editorial":

```sql
select * from libros
order by titulo,editorial;
```

Incluso, podemos ordenar en distintos sentidos, por ejemplo, por "titulo" en sentido ascendente y "editorial" en sentido descendente:

```sql
select * from libros
order by titulo asc, editorial desc;
```

Debe aclararse al lado de cada campo, pues estas palabras claves afectan al campo inmediatamente anterior.

Es posible ordenar por un campo que no se lista en la selección incluso por columnas calculados.

Se puede emplear "order by" con campos de tipo caracter, numérico y date.

## Ejercicios de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla y la creamos con la siguiente estructura:

```sql
drop table libros;

create table libros(
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);
```

Ingresamos algunos registros:

```sql
insert into libros
values('El aleph','Borges','Emece','10/10/1980',25.33);

insert into libros
values('Java en 10 minutos','Mario Molina','Siglo XXI','05/12/2005',50.65);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Emece','29/11/2000',19.95);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Planeta','27/11/2004',15);
```

Recuperamos los registros ordenados por el título:

```sql
select * from libros
order by titulo;
```

Ordenamos los registros por el campo "precio", referenciando el campo por su posición en la lista de selección:

```sql
select titulo, autor, precio
from libros order by 3;
```

Los ordenamos por "editorial", de mayor a menor empleando "desc":

```sql
select * from libros
order by editorial desc;
```

Ordenamos por dos campos:

```sql
select * from libros
order by titulo,editorial;
```

Ordenamos en distintos sentidos:

```sql
select * from libros
order by titulo asc, editorial desc;
```

Podemos ordenar por un campo que no se lista en la selección:

```sql
select titulo, autor
from libros
order by precio;
```

Está permitido ordenar por valores calculados, lo hacemos:

```sql
select titulo, editorial, precio+(precio*0.1) as "precio con descuento"
from libros
order by 3;
```

Ordenamos los libros por la fecha de edición:

```sql
select titulo, editorial, edicion
from libros
order by edicion;
```

Mostramos el título y año de edición de todos los libros, ordenados por año de edición:

```sql
select titulo, extract (year from edicion) as edicion
from libros
order by 2;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);

insert into libros
values('El aleph','Borges','Emece','10/10/1980',25.33);

insert into libros
values('Java en 10 minutos','Mario Molina','Siglo XXI','05/12/2005',50.65);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Emece','29/11/2000',19.95);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Planeta','27/11/2004',15);

select * from libros
order by titulo;

select titulo,autor,precio
from libros order by 3;

select * from libros
order by editorial desc;

select * from libros
order by titulo,editorial;

select * from libros
order by titulo asc, editorial desc;

select titulo, autor
from libros
order by precio;

select titulo, editorial, precio+(precio*0.1) as "precio con descuento"
from libros
order by 3;

select titulo, editorial, edicion
from libros
order by edicion;

select titulo, extract (year from edicion) as edicion
from libros
order by 2;
```

## Ejercicios propuestos

En una página web se guardan los siguientes datos de las visitas: nombre, mail, pais y fecha.

1. Elimine la tabla "visitas" y créela con la siguiente estructura:

```sql
drop table visitas;

create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);
```

2. Ingrese algunos registros:

```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina',to_date('2020/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@hotmail.com>','Chile',to_date('2020/02/13 11:08:10', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/02 14:12:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/06/17 20:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/02/08 20:05:40', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/06 18:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2019/10/05 23:00:00', 'yyyy/mm/dd hh24:mi:ss'));
```

3. Ordene los registros por fecha, en orden descendente.

4. Muestre el nombre del usuario, pais y el mes, ordenado por pais (ascendente) y el mes (descendente)

5. Muestre los mail, país, ordenado por país, de todos los que visitaron la página en octubre (4 registros)
